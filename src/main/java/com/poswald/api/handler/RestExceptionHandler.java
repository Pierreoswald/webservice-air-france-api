package com.poswald.api.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.poswald.core.dto.ErrorDto;
import com.poswald.core.exception.EntityNotFoundException;
import com.poswald.core.exception.InvalidEntityException;
import com.poswald.core.exception.InvalidOperationException;

/**
* Handler for rest error
* 
* @author Poswald
* 
*/
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<ErrorDto> handlerException(EntityNotFoundException entityNotFoundException, WebRequest webrequest){
		
		final HttpStatus notFound = HttpStatus.NOT_FOUND;
		
		final ErrorDto errorDto = ErrorDto.builder()
		.errorCode(entityNotFoundException.getErrorCode())
		.httpCode(notFound.value())
		.message(entityNotFoundException.getMessage())
		.build();
		
		return new ResponseEntity<ErrorDto>(errorDto,notFound);
	}
	
	@ExceptionHandler(InvalidOperationException.class)
	public ResponseEntity<ErrorDto> handleException(InvalidOperationException exception, WebRequest webRequest) {

		final HttpStatus notFound = HttpStatus.BAD_REQUEST;
	    final ErrorDto errorDto = ErrorDto.builder()
	        .errorCode(exception.getErrorCode())
	        .httpCode(notFound.value())
	        .message(exception.getMessage())
	        .build();

	    return new ResponseEntity<>(errorDto, notFound);
	}
	
	@ExceptionHandler(InvalidEntityException.class)
	public ResponseEntity<ErrorDto> handlerException(InvalidEntityException invalidEntityException, WebRequest webrequest){
		
		final HttpStatus badRequest = HttpStatus.NOT_ACCEPTABLE;
		final ErrorDto errorDto = ErrorDto.builder()
				.errorCode(invalidEntityException.getErrorCode())
				.httpCode(badRequest.value())
				.message(invalidEntityException.getMessage())
				.errors(invalidEntityException.getErrors())
				.build();
		
		return new ResponseEntity<ErrorDto>(errorDto,badRequest);
	}
	
}
