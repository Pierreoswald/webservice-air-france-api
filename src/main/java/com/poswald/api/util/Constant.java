package com.poswald.api.util;

public interface Constant {

	/**
	 * User endpoint
	 */
	public static String USER_ENDPOINT = "/user";

}
