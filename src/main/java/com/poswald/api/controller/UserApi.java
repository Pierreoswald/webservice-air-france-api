package com.poswald.api.controller;

import static com.poswald.api.util.Constant.USER_ENDPOINT;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.poswald.core.dto.UserDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
* Interface for User Controller
* 
* @author Poswald
* 
*/
@Api(USER_ENDPOINT)
public interface UserApi {

	@ApiOperation(value = "Register a user", notes = "Edit or add", response = UserDto.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "User created successfully"),
			@ApiResponse(code = 400, message = "data structure error"),
			@ApiResponse(code = 406, message = "Invalid user")
	})
	@PostMapping("/save")
	UserDto save(@RequestBody UserDto utilisateur);

	@ApiOperation(value = "Find a user", notes = "Search by id", response = UserDto.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "User found"),
			@ApiResponse(code = 404, message = "No user found"),
			@ApiResponse(code = 400, message = "Invalid id")
	})
	@GetMapping("/{id}")
	UserDto findById(@PathVariable Long id);

}
