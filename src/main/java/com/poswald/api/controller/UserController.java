package com.poswald.api.controller;

import static com.poswald.api.util.Constant.USER_ENDPOINT;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.poswald.core.dto.UserDto;
import com.poswald.core.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
* Controller for acces user management
* 
* @author Poswald
* 
*/
@Slf4j
@RestController
@RequestMapping(USER_ENDPOINT)
public class UserController implements UserApi {

	private UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Save (Edit or update) User
	 * 
	 * @param userDto: User to save
	 * @return User Dto update
	 */
	@Override
	public UserDto save(UserDto user) {
		log.info(Instant.now() + " --- Input: UserController save user: " + user);
		UserDto userSave = userService.save(user);
		log.info(Instant.now() + " --- Output: " + userSave);
		return userSave;
	}

	/**
	 * Find by id a User
	 * 
	 * @param id: identifiant of user to find
	 * @return User with the id
	 */
	@Override
	public UserDto findById(Long id) {
		log.info(Instant.now() + " --- Input: UserController findById id: " + id);
		UserDto user = userService.findById(id);
		log.info(Instant.now() + "--- Output UserController findById Output: " + user);
		return user;
	}

}
