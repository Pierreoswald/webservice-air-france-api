package com.poswald.core.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.poswald.core.dto.UserDto;
import com.poswald.core.exception.EntityNotFoundException;
import com.poswald.core.exception.ErrorCode;
import com.poswald.core.exception.InvalidEntityException;
import com.poswald.core.exception.InvalidOperationException;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserSeviceTest {
	
	@Autowired
	private UserService service;
	
	@Test
	public void shouldSaveUserWithSuccess() {
		
		Instant birthDate = LocalDateTime.now().minusYears(20).toInstant(ZoneOffset.UTC);
		
		UserDto expectedUser = UserDto.builder()
				.lastName("last name")
				.firstName("first name")
				.birthDate(birthDate)
				.country("France")
				.login("login")
				.email("email")
				.password("password")
				.build();

		UserDto savedUser = service.save(expectedUser);

	    assertNotNull(savedUser);
	    assertNotNull(savedUser.getId());
	    assertEquals(expectedUser.getLastName(), savedUser.getLastName());
	    assertEquals(expectedUser.getBirthDate(), savedUser.getBirthDate());
	    assertEquals(expectedUser.getFirstName(), savedUser.getFirstName());
	    assertEquals(expectedUser.getCountry(), savedUser.getCountry());
	    assertEquals(expectedUser.getLogin(), savedUser.getLogin());
	    assertEquals(expectedUser.getPassword(), savedUser.getPassword());
	    assertEquals(expectedUser.getEmail(), savedUser.getEmail());

	}
	
	@Test
	public void shouldUpdateUserWithSuccess() {
		
		Instant birthDate = LocalDateTime.now().minusYears(30).toInstant(ZoneOffset.UTC);
		
		UserDto expectedUser = UserDto.builder()
				.lastName("last name s")
				.firstName("first name s")
				.birthDate(birthDate)
				.country("France")
				.login("login")
				.email("email")
				.password("password")
				.build();

			
		UserDto savedUser = service.save(expectedUser);

		UserDto userToUpdate = savedUser;
			
		Instant birthDateUpdate = LocalDateTime.now().minusYears(40).toInstant(ZoneOffset.UTC);
		
		userToUpdate.setLastName("last name update");
		userToUpdate.setFirstName("first name update");
		userToUpdate.setBirthDate(birthDateUpdate);
		userToUpdate.setCountry("France");
		userToUpdate.setLogin("Login update");
		userToUpdate.setEmail("email update");
		userToUpdate.setPassword("password update");

		savedUser = service.save(userToUpdate);

		assertNotNull(savedUser);
		assertNotNull(savedUser.getId());
		assertEquals(userToUpdate.getLastName(), savedUser.getLastName());
		assertEquals(userToUpdate.getFirstName(), savedUser.getFirstName());
		assertEquals(userToUpdate.getCountry(), savedUser.getCountry());
	    assertEquals(userToUpdate.getBirthDate(), savedUser.getBirthDate());
	    assertEquals(userToUpdate.getLogin(), savedUser.getLogin());
	    assertEquals(userToUpdate.getPassword(), savedUser.getPassword());
	    assertEquals(userToUpdate.getEmail(), savedUser.getEmail());
	}
	
	@Test
	public void shouldSaveDefaultUserWithSuccess() {
		
		UserDto expectedUser = UserDto.builder()
				.activatedDefaultValue(true)
				.build();

		UserDto savedUser = service.save(expectedUser);

	    assertNotNull(savedUser);
	    assertNotNull(savedUser.getId());
	    assertEquals("Doe", savedUser.getLastName());
	    assertEquals("Jhon", savedUser.getFirstName());
	    assertEquals("France", savedUser.getCountry());
	    assertEquals("Login", savedUser.getLogin());
	    assertEquals("1234", savedUser.getPassword());
	    assertEquals("email", savedUser.getEmail());

	}
	
	@Test
	public void shouldThrowInvalidOperationException() {
		Assertions.assertThrows(InvalidOperationException.class, () -> {
			service.findById(0L);
	    });
		Assertions.assertThrows(InvalidOperationException.class, () -> {
			service.findById(-10L);
	    });
		Assertions.assertThrows(InvalidOperationException.class, () -> {
			service.findById(null);
	    });
	}
	
	@Test
	public void shouldThrowEntityNotFoundException() {
		Assertions.assertThrows(EntityNotFoundException.class, () -> {
			service.findById(9999L);
	    });
	}
	
	@Test
	public void shouldThrowInvalidEntityException() {
		UserDto expectedUser= UserDto.builder().birthDate(Instant.now().plus(10, ChronoUnit.DAYS)).build();

	    InvalidEntityException expectedException = assertThrows(InvalidEntityException.class, () -> service.save(expectedUser));

	    assertEquals(ErrorCode.USER_NOT_VALID, expectedException.getErrorCode());
	    assertEquals(6, expectedException.getErrors().size());
	    assertEquals("Please fill login user", expectedException.getErrors().get(0));
	    assertEquals("Please fill password user", expectedException.getErrors().get(1));
	    assertEquals("Please fill email user", expectedException.getErrors().get(2));
	    assertEquals("The date of birth is not valid", expectedException.getErrors().get(3));
	    assertEquals("You must be over 18", expectedException.getErrors().get(4));
	    assertEquals("You must live in france", expectedException.getErrors().get(5));
	}
	
	@Test
	public void shouldThrowInvalidEntityExceptionMaxSize() {
		
		Instant birthDate = LocalDateTime.now().minusYears(20).toInstant(ZoneOffset.UTC);
		
		UserDto expectedUser = UserDto.builder()
				.lastName("lastnameplgtopfgtfmnfjftdffrtcjfgtmvbdcjtgrerftgbtaz")
				.firstName("first name plgtopfgtfmnfjftdffrtcjfgtmvbdcjtgrerftgbtaz")
				.birthDate(birthDate)
				.country("France")
				.login("login plgtopfgtfmnfjftdffrtcjfgtmvbdcjtgrerftgbtazytrfg")
				.email("email plgtopfgtfmnfjftdffrtcjfgtmvbdcjtgrerftgbtazplgtopfgtfmnfjftdffrtcjfgtmvbdcjtgrerftgbtaz")
				.password("password")
				.build();

	    InvalidEntityException expectedException = assertThrows(InvalidEntityException.class, () -> service.save(expectedUser));

	    assertEquals(ErrorCode.USER_NOT_VALID, expectedException.getErrorCode());
	    assertEquals(4, expectedException.getErrors().size());
	    assertEquals("Please enter a login with a maximum of 50 characters", expectedException.getErrors().get(0));
	    assertEquals("Please enter a first name with a maximum of 50 characters", expectedException.getErrors().get(1));
	    assertEquals("Please enter a last name with a maximum of 50 characters", expectedException.getErrors().get(2));
	    assertEquals("Please enter a email with a maximum of 70 characters", expectedException.getErrors().get(3));

	}
}
